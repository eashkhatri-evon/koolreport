<?php
    require_once "../koolreport/core/autoload.php";
    use \koolreport\widgets\koolphp\Table;
    use \koolreport\widgets\google\ColumnChart;


switch($_GET['type']) {
	case 'noProfit':
		$data = array(
		array("category"=>"Books","sale"=>32000,"cost"=>20000),
		array("category"=>"Accessories","sale"=>43000,"cost"=>36000),
		array("category"=>"Phones","sale"=>54000,"cost"=>39000),
		array("category"=>"Movies","sale"=>23000,"cost"=>18000),
		array("category"=>"Others","sale"=>12000,"cost"=>6000)
	    );
	break;

	case 'profitFirst':
		$data = array(
		array("category"=>"Books","profit"=>12000,"sale"=>32000,"cost"=>20000),
		array("category"=>"Accessories","profit"=>7000,"sale"=>43000,"cost"=>36000),
		array("category"=>"Phones","profit"=>15000,"sale"=>54000,"cost"=>39000),
		array("category"=>"Movies","profit"=>5000,"sale"=>23000,"cost"=>18000),
		array("category"=>"Others","profit"=>6000,"sale"=>12000,"cost"=>6000)
 	    );
	break;
	default:
		$data = array(
		array("category"=>"Books","sale"=>32000,"cost"=>20000,"profit"=>12000),
		array("category"=>"Accessories","sale"=>43000,"cost"=>36000,"profit"=>7000),
		array("category"=>"Phones","sale"=>54000,"cost"=>39000,"profit"=>15000),
		array("category"=>"Movies","sale"=>23000,"cost"=>18000,"profit"=>5000),
		array("category"=>"Others","sale"=>12000,"cost"=>6000,"profit"=>6000)
	    );
		break;

		
}	
    
?>
<html>
    <head>
        <title>KoolReport's Widgets</title>
    </head>
    <body>
<div>
	<a href="/koolreport/examples/test.php">Default</a>&nbsp;|&nbsp;
	<a href="/koolreport/examples/test.php?type=profitFirst">Profit first</a>&nbsp;|&nbsp;
	<a href="/koolreport/examples/test.php?type=noProfit">No Profit</a>

</div>
        <?php 
        Table::create(array(
            "dataSource"=>$data
        ));
        ?>
        <?php 
        ColumnChart::create(array(
            "dataSource"=>$data
        ));
        ?>
    </body>
</html>

